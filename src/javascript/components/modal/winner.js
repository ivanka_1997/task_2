import { showModal } from "./modal";
export function showWinnerModal(fighter) {
  // call showModal function
  const title = `${fighter.name} is the winner!`;
  const bodyElement = "Close the window to start a new game.";
  const onClose = () => window.location.reload();
  showModal({ title, bodyElement, onClose });
}
