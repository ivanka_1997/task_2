import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let keysSet = new Set();
    // PlayerOneAttack: 'KeyA',
    // PlayerOneBlock: 'KeyD',
    // PlayerTwoAttack: 'KeyJ',
    // PlayerTwoBlock: 'KeyL',
    // PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
    // PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
    const keys = [
      "KeyA",
      "KeyD",
      "KeyJ",
      "KeyL",
      "KeyQ",
      "KeyW",
      "KeyE",
      "KeyU",
      "KeyI",
      "KeyO",
    ];

    let firstPlayer = Object.assign({}, firstFighter);
    let secondPlayer = Object.assign({}, secondFighter);
    let isCriticalHitFirstActive = true;
    let isCriticalHitSecondActive = true;
    firstPlayer.isCriticalHit = false;
    secondPlayer.isCriticalHit = false;
    document.addEventListener("keydown", function (e) {
      if (firstPlayer.health > 0 && secondPlayer.health > 0) {
        if (keys.includes(e.code)) {
          keysSet.add(e.code);
        }

        if (keysSet.size == 1) {
          if (keysSet.has(keys[0])) {
            firstPlayer.attack = getHitPower(firstFighter);
            let attack = getHitPower(firstFighter);
            secondPlayer.health -= attack;
            document.querySelector("#right-fighter-indicator").style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;
          }
          if (keysSet.has(keys[1])) {
            firstPlayer.defense = getBlockPower(firstFighter);
          }
          if (keysSet.has(keys[2])) {
            secondPlayer.attack = getHitPower(secondFighter);
            firstPlayer.health -= secondPlayer.attack;
            document.querySelector("#left-fighter-indicator").style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;
          }
          if (keysSet.has(keys[3])) {
            secondPlayer.defense = getBlockPower(secondFighter);
          }
        }

        if (
          (keysSet.has(keys[0]) && keysSet.has(keys[1])) ||
          (keysSet.has(keys[2]) && keysSet.has(keys[3]))
        ) {
          console.log("cant block and attack");
        }
        if (keysSet.has(keys[0]) && keysSet.has(keys[3])) {
          if (keysSet.entries().next().value[0] == keys[3]) {
            console.log("block is equel or bigger then attack");
          } else {
            let damage = getDamage(firstFighter, secondFighter);
            secondPlayer.health -= damage;
            document.querySelector("#right-fighter-indicator").style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;
          }
        }
        if (keysSet.has(keys[1]) && keysSet.has(keys[2])) {
          if (keysSet.entries().next().value[0] == keys[1]) {
            console.log("block is equel or bigger then attack");
          } else {
            let damage = getDamage(secondFighter, firstFighter);
            firstPlayer.health -= damage;
            document.querySelector("#left-fighter-indicator").style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;
          }
        }

        ///critical
        if (keysSet.size == 3) {
          if (
            keysSet.has(keys[4]) &&
            keysSet.has(keys[5]) &&
            keysSet.has(keys[6]) &&
            isCriticalHitFirstActive
          ) {
            firstPlayer.attack = firstFighter.attack * 2;
            secondPlayer.health -= firstPlayer.attack;
            document.querySelector("#right-fighter-indicator").style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;

            isCriticalHitFirstActive = false;
            firstPlayer.isCriticalHit = true;
            setTimeout(() => {
              isCriticalHitFirstActive = true;
            }, 10000);
          }

          if (
            keysSet.has(keys[7]) &&
            keysSet.has(keys[8]) &&
            keysSet.has(keys[9]) &&
            isCriticalHitSecondActive
          ) {
            secondPlayer.attack = secondFighter.attack * 2;

            firstPlayer.health -= secondPlayer.attack;
            document.querySelector("#left-fighter-indicator").style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;

            isCriticalHitSecondActive = false;
            secondPlayer.isCriticalHit = true;
            setTimeout(() => {
              isCriticalHitSecondActive = true;
            }, 10000);
          }
          if (firstPlayer.isCriticalHit) secondPlayer.defense = 0;
          if (secondPlayer.isCriticalHit) firstPlayer.defense = 0;
        }
      }
      if (firstPlayer.health <= 0) resolve(secondFighter);
      if (secondPlayer.health <= 0) resolve(firstFighter);
    });

    document.addEventListener("keyup", function (e) {
      if (keys.includes(e.code)) {
        keysSet.delete(e.code);
      }
    });
  });
}
export function getDamage(attacker, defender) {
  // return damage
  let demage = getHitPower(attacker) - getBlockPower(defender);
  return demage <= 0 ? 0 : demage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}
