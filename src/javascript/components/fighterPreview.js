import { createElement } from "../helpers/domHelper";

export function createFighterPreview(fighter, position) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter != undefined) {
    fighterElement.innerHTML = `
  <h3>${fighter.name}</h3>
  <p><span>Health:</span> <span class = 'fighter-preview___info'> ${fighter.health}</span></p>
  <p><span>Attack: </span> <span class = 'fighter-preview___info'> ${fighter.attack}</span></p>
  <p><span>Defense: </span> <span class = 'fighter-preview___info'> ${fighter.defense}</span></p>
  `;
    const FighterImg = createFighterImage(fighter);
    fighterElement.append(FighterImg);
  }
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
